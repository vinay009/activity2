//
//  ViewController.swift
//  Activity2
//
//  Created by Vishwanath Keerthi on 2019-06-28.
//  Copyright © 2019 Vishwanath Keerthi. All rights reserved.
//  send message to watch

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Check if the phone supports WCSession
        // Need WCSession to communicate with a watch
        if (WCSession.isSupported()) {
            print("PHONE: Yes phone supports SESSION!")
            let session = WCSession.default
            session.delegate = self as! WCSessionDelegate
            session.activate()
        }
        else {
            print("PHONE: Phone does NOT support SESSION")
        }
    }


    @IBAction func ButtonClicked(_ sender: Any) {
    }
    

}

